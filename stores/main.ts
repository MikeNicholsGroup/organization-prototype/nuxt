import { defineStore } from 'pinia'
// main is the name of the store. It is unique across your application
// and will appear in devtools
export const useMainStore = defineStore('main', {
  // a function that returns a fresh state
  state: () => ({
    counter: 99,
    base: '/organization-prototype/prototype/',
    section: {
      org: 'a',
      id: 'org',
      label: 'Produce Inc',
      icon: 'a-org'
    },
    url: 'a-org-overview',
    isSwitcherOpen: false,
    notinprototype: 'one'
  }),

  // optional getters
  getters: {
    // getters receive the state as first parameter
    doubleCounter: (state) => state.counter * 2,
    // use getters in other getters
    doubleCounterPlusOne(): number {
      return this.doubleCounter + 1
    },
  },
  // optional actions
  actions: {
    setSwitcherOpen( onoff:boolean ) {      
      this.isSwitcherOpen = onoff;
    },
    onNavigate(dest:{section:{org:string, id:string, label:string, icon:string}, url:string}) {
      if (dest.url) {
        this.url = dest.url;
      }else {
        console.log('Not in prototype');
        if (this.notinprototype === 'flashone'){
          this.notinprototype = 'flashtwo'
        } else {
          this.notinprototype = 'flashone'
        }  
      }
      if (dest.section) {
        this.section = dest.section;
      }
      this.isSwitcherOpen = false; 
    },
  },
})