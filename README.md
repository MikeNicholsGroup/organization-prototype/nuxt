## Options

- Flyout(default): https://mikenicholsgroup.gitlab.io/organization-prototype/prototype/
- Modal: https://mikenicholsgroup.gitlab.io/organization-prototype/prototype/?switcher=modal
- Slide: https://mikenicholsgroup.gitlab.io/organization-prototype/prototype/?switcher=slide
- Super switcher: https://mikenicholsgroup.gitlab.io/organization-prototype/prototype/?switcher=super

Look at the [Content documentation](https://content-v2.nuxtjs.org/) to learn more.

## Edit content
To edit content follow instuction [here](https://gitlab.com/MikeNicholsGroup/organization-prototype/prototype/-/issues/1)

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
yarn run dev
```

## Production

Build the application for production:

```bash
yarn generate
```

Locally preview production build:

```bash
npm run preview
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/docs/deployment) for more information.
