// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: ['@nuxt/content', '@pinia/nuxt'],
  app: {
    baseURL: '/organization-prototype/prototype/',
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      link: [
        { rel: 'preconnect', href: 'https://rsms.me/' },
        { rel: 'preconnect', href: 'https://rsms.me/inter/inter.css' }
      ],
    }
  },
})

