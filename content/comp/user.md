---
a:
    issues: 27
    mergerequests: 4
    todo: 99
    switcher:
        projects:
            project1:
                label: Apples
                desc: Produce Inc
                icon: a-project-apples
                url: a-project-apples
                section:
                    org: a
                    id: project
                    label: Apples
                    icon: a-project-apples
            project2:
                label: Oranges
                desc: Produce Inc
                icon: a-project-oranges
                url: a-project-oranges
                section:
                    org: a
                    id: project
                    label: Oranges
                    icon: a-project-oranges
            project3:
                label: Lemons
                desc: Produce Inc
                icon: a-project-lemons
                url: a-project-lemons
                section:
                    org: a
                    id: project
                    label: Lemons
                    icon: a-project-lemons
            project4:
                label: Kiwis
                desc: Produce Inc
                icon: a-project-kiwis
                url: a-project-kiwis
                section:
                    org: a
                    id: project
                    label: Kiwis
                    icon: a-project-kiwis
            project5:
                label: Watermelons
                desc: Produce Inc
                icon: a-project-watermelons
                url: a-project-watermelons
                section:
                    org: a
                    id: project
                    label: Watermelons
                    icon: a-project-watermelons
        groups:
            group1:
                label: Fruits
                desc: Produce Inc
                icon: a-group-fruits
                url: a-group-fruits
                section:
                    org: a
                    id: group
                    label: Fruits
                    icon: a-group-fruits
            group2:
                label: Vegetables
                desc: Produce Inc
                icon: a-group-vegetables
                url: a-group-vegetables
                section:
                    org: a
                    id: group
                    label: Vegetables
                    icon: a-group-vegetables
            group3:
                label: Services
                desc: Produce Inc
                icon: a-group-services
                url: a-group-services
                section:
                    org: a
                    id: group
                    label: Services
                    icon: a-group-services
b:
    issues: 4
    mergerequests: 1
    todo: 2
    switcher:
        projects:
            project1:
                label: Tree
                icon: b-project-tree
                url: b-project-tree
                section:
                    org: b
                    id: project
                    label: Tree
                    icon: b-project-tree
            project2:
                label: PalmTree
                icon: b-project-palmtree
                url: b-project-palmtree
                section:
                    org: b
                    id: project
                    label: PalmTree
                    icon: b-project-palmtree
            project3:
                label: Plant
                icon: b-project-plant
                url: b-project-plant
                section:
                    org: b
                    id: project
                    label: Plant
                    icon: b-project-plant
            project4:
                label: Flower
                icon: b-project-flower
                url: b-project-flower
                section:
                    org: b
                    id: project
                    label: Flower
                    icon: b-project-flower
        groups:
            group1:
                label: Arborist
                icon: b-group-arborist
                url: b-group-arborist
                section:
                    org: b
                    id: group
                    label: Arborist
                    icon: b-group-arborist
            group2:
                label: Botanist
                icon: b-group-botanist
                url: b-group-botanist
                section:
                    org: b
                    id: group
                    label: Botanist
                    icon: b-group-botanist
c:
    issues: 2
    mergerequests: 0
    todo: 1
    switcher:
        projects:
            project1:
                label: Hammer
                icon: c-project-hammer
                url: c-project-hammer
                section:
                    org: c
                    id: project
                    label: Hammer
                    icon: c-project-hammer
            project2:
                label: Saw
                icon: c-project-saw
                url: c-project-saw
                section:
                    org: c
                    id: project
                    label: Saw
                    icon: c-project-saw
            project3:
                label: Wrench
                icon: c-project-wrench
                url: c-project-wrench
                section:
                    org: c
                    id: project
                    label: Wrench
                    icon: c-project-wrench
        groups:
            group1:
                label: Construction
                icon: c-group-construction
                url: c-group-construction
                section:
                    org: c
                    id: group
                    label: Construction
                    icon: c-group-construction
---
User info