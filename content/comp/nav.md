---
yourwork:
    pages:
      projects:
        title: Projects
        icon: project
        url: projects
      groups:
        title: Groups
        icon: group
        url: groups
      issues:
        title: Issues
        icon: issues
        url: 
      mergerequests:
        title: Merge requests
        icon: merge-request
        url: 
      todolist:
        title: To-Do List
        icon: todo-done
        url: 
      milestones:
        title: Milestones
        icon: clock
        url: 
      snippets:
        title: Snippets
        icon: snippet
        url: 
      activity:
        title: Activity
        icon: history
        url: 
      environments:
        title: Environments
        icon: environment
        url: 
      operations:
        title: Operations
        icon: cloud-gear
        url: 
      security:
        title: Security
        icon: shield
        url: 
project:
  pages:
    overview:
      title: Project overview
      icon: project
      url: 
    issues:
      title: Issues
      icon: issues
      url: 
    mergerequests:
      title: Merge requests
      icon: merge-request
      url: 
    manage:
      title: Manage
      icon: users
      url: 
      expandable: true
    plan:
      title: Plan
      icon: planning
      url: 
      expandable: true
    code:
      title: Code
      icon: code
      url: 
      expandable: true
    build:
      title: Build
      icon: rocket
      url: 
      expandable: true
    secure:
      title: Secure
      icon: shield
      url: 
      expandable: true
    operate:
      title: Operate
      icon: deployments
      url: 
      expandable: true
    monitor:
      title: Monitor
      icon: monitor
      url: 
      expandable: true
    analyze:
      title: Analyze
      icon: chart
      url: 
      expandable: true
    settings:
      title: Settings
      icon: settings
      url: 
      expandable: true
group:
  pages:
    overview:
      title: Group overview
      icon: project
      url:
    issues:
      title: Issues
      icon: issues
      url: 
    mergerequests:
      title: Merge requests
      icon: merge-request
      url: 
    manage:
      title: Manage
      icon: users
      url: 
      expandable: true
    plan:
      title: Plan
      icon: planning
      url: 
      expandable: true
    build:
      title: Build
      icon: rocket
      url: 
      expandable: true
    operate:
      title: Operate
      icon: deployments
      url: 
      expandable: true
    settings:
      title: Settings
      icon: settings
      url: 
      expandable: true
explore:
  pages:
    groups:
      title: Explore page
      icon: group
      url: 
org:
  pages:
    overview:
      title: Organization overview
      icon: organization
      url: overview
    activity:
      title: Activity
      icon: history
      url: activity
    directory:
      title: Directory
      icon: list-bulleted
      url: directory
    manage:
      title: Manage
      icon: users
      url:
      expandable: true
    billing:
      title: Billing
      icon: none
      url: billing
      expandable: false
    settings:
      title: Settings
      icon: settings
      url:
      expandable: true
    visibility:
      title: Visibility
      icon: none
      url: visibility
      expandable: false
---
Nav pages 