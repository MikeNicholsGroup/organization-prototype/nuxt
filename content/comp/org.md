---
orgs:
  orga:
    section:
      org: a
      id: org
      label: Produce Inc
      icon: a-org
    nav:
      title: Produce Inc
      icon: a-org
      url: a-org-overview
  orgb:
    section:
      org: b
      id: org
      label: Vegetation Co
      icon: b-org
    nav:
      title: Vegetation Co
      icon: b-org
      url: b-org-overview
  orgc:
    section:
      org: c
      id: org
      label: ToolLab
      icon: c-org
    nav:
      title: ToolLab
      icon: c-org
      url: c-org-overview
---
Organizations